extends KinematicBody2D

export (int) var player_id = 0
export (bool) var lamp = false
export (int) var speed = 3200
export (int) var jump_speed = -400
export (int) var gravity = 1000
export (String,FILE) var explosion = "res://objects/explosion/explosion.tscn"
export (String,FILE) var explosion2 = "res://objects/explosion/explosion.tscn"

onready var ray = $RayCast2D
onready var ray2 = $RayCast2D2
onready var start_position = position
onready var start_size = scale

var shrunk = false
var score = 0

var velocity = Vector2.ZERO

onready var sprite=$sprite

func _ready():
	load_settings()
	
func load_settings():
	#set_color(.8)
	if lamp:
		$Light2D.visible = true
	else:
		$Light2D.visible = false
		
func _physics_process(delta):
	get_input(delta)
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)
	jump_attack()

func set_score(value = 1):
	score+=value


func set_color():
	var v = float(player_id*.1)
	# Duplicate the shader so that changing its param doesn't change it on any other sprites that also use the shader.
	# Generally done once in _ready()
	sprite.set_material(sprite.get_material().duplicate(true))

	# Offset sprite hue by a random value within specified limits.
	#var rand_hue = float(randi() % 3)/2.0/3.2
	sprite.material.set_shader_param("Shift_Hue", v)

func jump_attack():
	if ray.is_colliding():
		attack(ray.get_collider()) 

	elif ray2.is_colliding():
		attack(ray2.get_collider()) 

func attack(obj):
	if obj.has_method("take_damage"):
		obj.take_damage(1)
		velocity.y = jump_speed / 2

func damage():
	var explode = load(explosion).instance()
	get_tree().get_current_scene().add_child(explode)
	explode.scale = Vector2(.5,.5)
	explode.position = position
	position = Vector2(0,0)
	$respawn_timer.start()
	
func walk_animation():
	if lamp:
		sprite.play("walk_lamp")
	else:
		sprite.play("walk")

		
func get_input(delta):
	velocity.x = 0
	
	var direction = Input.get_joy_axis(player_id,0)
	var DPAD_RIGHT = Input.is_joy_button_pressed(player_id,Controls.buttons.RIGHT)
	var DPAD_LEFT = Input.is_joy_button_pressed(player_id,Controls.buttons.LEFT)
	var left = Input.is_action_pressed("ui_left")
	var right = Input.is_action_pressed("ui_right")
	
	Global.debug_text = direction
	if direction > 0.5 || right || DPAD_RIGHT:
		sprite.flip_h = false
		walk_animation()	
		velocity.x += speed * delta * 2 * 1
	if direction < -0.5 || left || DPAD_LEFT:
		sprite.flip_h = true
		walk_animation() 
		velocity.x -= speed * delta * 2 * 1
	
		

		
	if velocity.x < 30 && velocity.x > -30:
		if lamp:
			sprite.play("default_lamp")
		else:
			sprite.play("default")
		
	if Input.is_action_just_pressed("jump")|| Input.is_joy_button_pressed(player_id,Controls.buttons.JUMP):
		if is_on_floor():
			$jump_snd.play()
			velocity.y = jump_speed


func _on_Area2D_body_entered(body):

	if body.has_method("collect"):
		body.collect()


func _on_respawn_timer_timeout():
	position = start_position;

func shrink():
	if shrunk:
		return
	shrunk = true
	set_collision_mask_bit(2,false)
	var explode = load(explosion2).instance()
	get_tree().get_current_scene().add_child(explode)
	explode.position = position
	scale = Vector2(1,1)
	$SizeTimer.start()

func _on_SizeTimer_timeout():
	shrunk = false
	set_collision_mask_bit(2,true)
	scale = start_size

func _input(event):
	if event.is_action_pressed("restart"):
		get_tree().reload_current_scene()
