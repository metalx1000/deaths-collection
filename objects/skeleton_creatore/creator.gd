extends Node2D

export var max_count = 5
export var wait_time = 3
export var skeletons = preload("res://objects/super_grotto/enemies/enemy_skell_fall.tscn")

func _ready():
	$Timer.wait_time = wait_time


func _on_Timer_timeout():
	if max_count < 1:
		return
		
	max_count -= 1
	var skeleton = skeletons.instance()
	get_tree().get_current_scene().add_child(skeleton)
	skeleton.position = position
