extends Node2D

var object
var clone = preload("res://objects/super_grotto/enemies/enemy_skell_fall.tscn")
var clone_pos

func _ready():
	pass # Replace with function body.


func idle():
	$AnimationPlayer.play("idle")


func _on_Area2D_body_entered(body):
	if body.is_in_group("souls") && body.full_size || body.is_in_group("players"):
		object = body
		$AnimationPlayer.play("smash")

func smash():
	$smasher/CollisionShape2D.disabled = false
	if object == null:
		return
		
	if object.is_in_group("souls"):
		clone_pos = object.position
		object.collect()
		create_mini()
		create_mini()
		create_mini()
		create_mini()
	
func create_mini():
	var mini = clone.instance()
	get_tree().get_current_scene().add_child(mini)
	mini.full_size = false
	mini.position = clone_pos
	mini.scale = Vector2(.5,.5)
	randomize()
	mini.velocity.y = rand_range(-250,-150)
	mini.velocity.x = rand_range(-150,150)

func smasher_off():
	$smasher/CollisionShape2D.disabled =true
	
func _on_smasher_body_entered(body):
	if body.is_in_group("players"):
		body.damage()
