extends KinematicBody2D


export (bool) var active = false
export (int) var speed = 1000
export (int) var gravity = 1000
export (int) var direction = 1
export (bool) var can_fall = false
export (bool) var sprite_flip = false
export (bool) var collectable = true
export (int) var updown = 0
export (int) var health = 1
export (int) var wall_check_updown = 0
export (bool) var is_demon = false

var full_size = true
var updown_wait = 0
var velocity = Vector2.ZERO

export (String,FILE) var explosion = "res://objects/explosion/explosion.tscn"
#var explosion = load("res://objects/explosion/explosion_2.tscn")
#var ex = load(ex)


onready var sprite = $sprite
onready var ray = $RayCast2D
onready var ground_check_r = $ground_check_r
onready var ground_check_l = $ground_check_l
onready var offscreen_timer =$offscreen_timer
onready var ceiling_check = $ceiling_check

var mover = Vector2.ZERO

func _ready():
	randomize()
	ray.cast_to.x *= direction
	if !can_fall:
		if sprite.material:
			sprite.material.set_shader_param("Shift_Hue", 0.5)
	
func  _physics_process(delta):
	velocity.x = 0
	if position.y > 2000:
		collect()
		
	if active:
		walk(delta)
		move_updown(delta)
		velocity.y += gravity * delta
		velocity = move_and_slide(velocity, Vector2.UP)
		
		
func turn():
	direction *= -1
	ray.cast_to.x *= -1

func move_updown(delta):
	if updown == 0:
		return
	
	updown_wait -= 1
	if updown_wait < 0:
		updown_wait = 100
		if ground_check_l.is_colliding() || ground_check_r.is_colliding():
			updown *= -1
		if ceiling_check.is_colliding():
			updown *= -1
		
	velocity.y += updown * delta
	
func direction():
	if direction == 1:
		if sprite_flip:
			sprite.flip_h = true
		else:
			sprite.flip_h = false
	else:
		if sprite_flip:
			sprite.flip_h = false
		else:
			sprite.flip_h = true
	
	sprite.play("walk") 
	
func wall_check(offset):
	offset = rand_range(-10,10)
	ray.cast_to.y = offset
	if ray.is_colliding():
		turn()
		
func walk(delta):
	if rand_range(0,3) > 2:
		wall_check(wall_check_updown)
	if rand_range(0,3) > 2:
		wall_check(-wall_check_updown)
	if rand_range(0,3) > 2:
		wall_check(0)
	
	if !can_fall:
		if !ground_check_l.is_colliding() || !ground_check_r.is_colliding():
			turn()
	
	
	direction()
		
	velocity.x += speed * delta * 2 * direction
	


func _on_VisibilityNotifier2D_viewport_entered(viewport):
	offscreen_timer.stop()
	active = true


func _on_VisibilityNotifier2D_viewport_exited(viewport):
	offscreen_timer.start()


func _on_offscreen_timer_timeout():
	#if gunner us off screen for 5 seconds then set to inactive
	active = false

func take_damage(amount):
	health -= 1
	if health < 1:
		collect()

func collect():
	if !collectable:
		return
	var explode = load(explosion).instance()
	get_tree().get_current_scene().add_child(explode)
	explode.position = position
	queue_free()


func _on_Area2D_area_shape_entered(area_id, area, area_shape, self_shape):
	if area.is_in_group("explosion"):
		collect()


func _on_attacker_body_entered(body):
	if !body.is_in_group("players"):
		return
		
	if is_demon:
		body.shrink()
	else:
		body.damage()
