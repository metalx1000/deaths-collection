extends Area2D


export var id = 0

func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Teleporter_body_entered(body):
	var landings = get_tree().get_nodes_in_group("landing_pads")
	for pad in landings:
		if pad.id == id:
			body.position = pad.position
			return
