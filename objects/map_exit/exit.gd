extends Node2D

export (String,FILE) var next_scene = "res://scenes/Title_Menu.tscn"

var active = false

func _ready():
	modulate.a = 0
	visible = false
	pass # Replace with function body.

func _process(delta):
	var count = get_tree().get_nodes_in_group("souls").size();
	if count < 1 && !active:
		get_tree().get_nodes_in_group("music")[0].stop()
		$Timer.start()
		$sound.play()
		active = true
		visible = true
		$AnimationPlayer.play("default")

func _on_Area2D_body_entered(body):
	if !active:
		return 
		
	if body.is_in_group("players"):
		get_tree().change_scene(next_scene)
		#exit_map()
		

func exit_map():
	var map = get_tree()
	var tween = get_tree().create_tween()
	tween.tween_property(map, "modulate", Color(1, 1, 1, 0), 2.0)
	tween.start()
	#tween.tween_callback(get_tree().change_scene(next_scene))

func _on_Timer_timeout():
	get_tree().get_nodes_in_group("music")[0].play()

func _input(event):
	if event.is_action_pressed("next_level"):
		get_tree().change_scene(next_scene)
