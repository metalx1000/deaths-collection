extends Node


var player_count = 1
var player_clone = preload("res://objects/death/player.tscn")
var debug_text = "TEST"

func _ready():
	
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
		

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()
		
func _input(event):
	var id = event.device
	load_players(id)
	
func load_players(id):
	#check if there is at lest 1 player (make sure we are in a level)
	if get_tree().get_nodes_in_group("players").size() < 1:
		return

	#if it's is not the jump button 
	#or left/right movement
	#being pressed don't continue
	var lr = abs(Input.get_joy_axis(id,0))
	var DPAD_RIGHT = Input.is_joy_button_pressed(id,JOY_DPAD_RIGHT)
	var DPAD_LEFT = Input.is_joy_button_pressed(id,JOY_DPAD_LEFT)
	var jump_btn = Input.is_joy_button_pressed(id,JOY_BUTTON_0)
	
	if DPAD_RIGHT||DPAD_LEFT||jump_btn|| lr > 0.5:
		
		#check and make sure that this player doesn't already exist

		var players = get_tree().get_nodes_in_group("players")
		for player in players:
			if player.player_id == id:
				return
	
		var player1 = get_tree().get_nodes_in_group("players")[0]
		var clone = player_clone.instance()
	
		clone.lamp = player1.lamp
		clone.player_id = id
		get_tree().get_current_scene().add_child(clone)
		clone.set_color()
		clone.position = player1.start_position
		clone.start_position = player1.start_position
		clone.load_settings()
