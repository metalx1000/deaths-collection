extends AnimatedSprite


export (String,FILE) var explosion = "res://objects/explosion/explosion.tscn"

func _ready():
	pass # Replace with function body.


func damage():
	var explode = load(explosion).instance()
	get_tree().get_current_scene().add_child(explode)
	
	explode.position = position
	queue_free()


func _on_Area2D_body_entered(body):
	damage()
