extends Control


export (String,FILE) var next_scene


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func continue():
	get_tree().change_scene(next_scene)

func collect():
	$sound.play()
	$collect.visible = true
	$collect.play("default")
