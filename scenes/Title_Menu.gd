extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		$fade.play("fade_out")
		
func next_scene():
	get_tree().change_scene("res://scenes/cut_scene_01/cut_scene_01.tscn")

func credits():
	get_tree().change_scene("res://scenes/Credts.tscn")
