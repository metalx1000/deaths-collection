extends Control

var buttons = []
var i = 0
var last_button

export var next_scene = "res://maps/map_01.tscn"

func _ready():
	for b in Controls.buttons:
		buttons.append(str(b))

	request_button()
	
func _input(event):
	if event is InputEventJoypadButton && event.is_pressed():
		$Label2.text = "Button " + str(event.button_index) + " Pressed"
		if event.button_index != last_button:
			last_button = event.button_index
			return
			
		Controls.buttons[buttons[i]] = event.button_index
		i+=1
		if i >= buttons.size():
			print($Label.text)
			Controls.save()
			get_tree().change_scene(next_scene)
		else:
			request_button()
		
func request_button():
	$Label.text = "Press Button For: "+buttons[i]+"\n"
	var text = ""
	if i != 0:
		text = buttons[i-1] + " set to " + str(Controls.buttons[buttons[i-1]])
		$Label.text += text
		print(text)
	
