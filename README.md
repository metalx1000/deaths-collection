# Death's Collection

A Game where you are Death and you collect souls

Copyright Kris Occhipinti 2022-02-14
(https://filmsbykris.com)
License GPLv3


# Fonts
Adventurer by Brian J Smith
license : (Creative Commons Attribution)
https://www.pentacom.jp/pentacom/bitfontmaker2/gallery/?id=195

# Sprites - tiles
Ansimuz
Artwork created by Luis Zuno (@ansimuz)
https://www.patreon.com/posts/death-lamp-enemy-84137238
https://www.patreon.com/posts/ogre-animations-85654635
https://www.patreon.com/posts/skeleton-enemy-72843464
https://www.patreon.com/posts/enemies-update-73345091

LICENSE:
You may use these assets in personal or commercial projects. You can modify these assets to suit your needs. You can re-distribute the file.
Credit no required but appreciated it.
____________________

LINKS
Twitter @ansimuz
Support my work at Patreon https://www.patreon.com/ansimuz
Buy my stuff https://ansimuz.itch.io/
Get more Free Assetslike these at: http://www.ansimuz.com
____________________

# Music
Music By https://teknoaxe.com/
Music is licensed under a Creative Commons Attribution 4.0 International License
https://creativecommons.org/licenses/by/4.0/
- https://teknoaxe.com/Link_Code_3.php?q=1971

Music by
Lesiakower from Pixabay
https://pixabay.com/users/lesiakower-25701529/?utm_source=link-attribution&utm_medium=referral&utm_campaign=music&utm_content=112527
https://pixabay.com/music/video-games-item-obtained-123644/
GeoffreyBurch from Pixabay
https://pixabay.com/users/geoffreyburch-5739114/

# Sounds
https://pixabay.com/sound-effects/interface-trial-124464/
